import Vue from 'vue'
import VueRouter from 'vue-router'

import LoginView from '../views/LoginView.vue'

import PatientView from '../views/pages/Patient.vue'
import AppointmentView from '../views/pages/Appointment.vue'
import OperatorsView from '../views/pages/Operators.vue'
import ProfileView from '../views/pages/Profile.vue'
import mutuelleView from '../views/pages/Mutuelle.vue'

import UsersViews from '../views/pages/Users.vue'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'login',
    meta: {layout:'blank'},
    component: LoginView
  },

  {
    path: '/dashboard',
    name: 'PatientView',
    component: PatientView
  },
  {
    path: '/appointment',
    name: 'appointment',
    component: AppointmentView
  },
  {
    path: '/operators',
    name: 'operators',
    component: OperatorsView
  },
  {
    path: '/profile',
    name: 'profile',
    component: ProfileView
  },
  {
    path: '/users',
    name: 'UsersViews',
    component: UsersViews
  },
  {
    path: '/mutuelle',
    name: 'mutuelleView',
    component: mutuelleView
  },
  
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
